#ifndef MAKE_XSEC_PLOT
#define MAKE_XSEC_PLOT

#include <iostream>
#include <TGraph.h>
#include <TH2F.H>
#include <TString.h>
#include <TCanvas.h>
#include <TGraphErrors.h>

#include <map>

class Xsec
{
public:
  Xsec() {}
  ~Xsec(){};
  bool ReadFile(TString filename = "", bool relunc = true, bool ispb=true);
  void SetFileName(TString name = "") {m_filename = name;}
  TString m_filename;
  TGraphErrors m_graph;
};

class make_xsec_plot
{
public:
  make_xsec_plot() {};
  ~make_xsec_plot() {};

  TCanvas * Draw();
  bool AddObject(TString key, TString filename, bool relunc = true, bool ispb = true);
  void Reset() {m_map.clear();}

private:
  std::map<TString, Xsec> m_map;

};


  
#endif // MAKE_XSEC_PLOT
