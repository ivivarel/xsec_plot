TCanvas * doPlot()
{
  make_xsec_plot * plotting = new make_xsec_plot;
  plotting->AddObject("#tilde{g}#tilde{g} (NNLO_{approx} + NNLL)","gluino_xsec.txt");
  plotting->AddObject("#tilde{q}#tilde{q} (NNLO_{approx} + NNLL, 5 flavours)","squark_xsec.txt");
  plotting->AddObject("#tilde{t}#tilde{t} (NNLO_{approx} + NNLL)","stop_xsec.txt");
  plotting->AddObject("#tilde{#chi}_{1}^{#pm}#tilde{#chi}_{2}^{0} (NLO + NLL, wino-like)","wino_xsec.txt", false, false);
  plotting->AddObject("#tilde{#chi}_{1}^{#pm}#tilde{#chi}_{2}^{0} (NLO + NLL, higgsino-like)","higgsino_xsec.txt", false, false);

  TCanvas * mycan = plotting->Draw();
  
  return mycan;
}
  
