#include "make_xsec_plot.h"
#include <fstream>

#include <TLatex.h>
#include <TLegend.h>
#include <TColor.h>
#include <TLegendEntry.h>

bool Xsec::ReadFile(TString filename, bool relunc, bool ispb)
{
  ifstream ifile;
  ifile.open(filename);
  if (!ifile.is_open()) return false;

  std::vector<double> v_mass;
  std::vector<double> v_xsec;
  std::vector<double> v_unc;
  
  double mass;
  double xsec;
  double unc;

  while (ifile >> mass >> xsec >> unc){
    v_mass.push_back(mass);
    if (!ispb){
      xsec = xsec/1000.;
      if (!relunc) unc = unc/1000.;
    }
    v_xsec.push_back(xsec);
    if (!relunc) v_unc.push_back(unc);
    else v_unc.push_back((unc/100.) * xsec);
  }

  std::cout << "Read " << v_mass.size() << " points" << std::endl;

  m_graph.Set(v_mass.size());
  
  
  for (unsigned int i = 0; i < v_mass.size(); ++i){
    m_graph.SetPoint(i,v_mass[i],v_xsec[i]);
    m_graph.SetPointError(i,v_unc[i],v_unc[i]);
    std::cout << v_mass[i] << '\t' << v_xsec[i] << '\t' << v_unc[i] << std::endl;
  }
  
  return true;
}

TCanvas * make_xsec_plot::Draw()
{
  if (m_map.empty()){
    std::cout << "Sorry, there is nothing to draw. Did you added objects with AddObject?" << std::endl;
    return 0;
  }

  TCanvas * retval = new TCanvas("xsec_plot");
  retval->SetGrid();
  retval->SetLogy();
  retval->SetTopMargin(0.1);
  TH2F * h_axes = new TH2F("h_axes","",100,0,3000,100,0.00001,1000000);
  h_axes->SetXTitle("SUSY particle mass [GeV]");
  h_axes->SetYTitle("Cross-section [pb]");
  h_axes->Draw();

  TLatex * l_latex = new TLatex(0.7,0.92,"pp, #sqrt{s} = 13 TeV");
  l_latex->SetNDC();
  l_latex->Draw();

  std::map<TString, Xsec>::iterator l_it;

  unsigned int i = 0;

  TLegend * theleg = new TLegend(0.48,0.45,0.93,0.85);
  theleg->SetFillStyle(0);
  theleg->SetBorderSize(1);
  theleg->SetLineWidth(0);


  std::map<int,int> color_map;
  color_map.emplace(0,kBlue+2);
  color_map.emplace(1,kAzure+1);
  color_map.emplace(2,kMagenta+2);
  color_map.emplace(3,kRed);
  color_map.emplace(4,kMagenta-9);

    
  
  for (l_it = m_map.begin(); l_it != m_map.end(); ++l_it){
    TGraphErrors * the_graph = &((l_it->second).m_graph);
    int mycolour;
    if (color_map.size() > i) mycolour = color_map[i];
    else mycolour = i;
    the_graph->SetLineColor(mycolour);
    the_graph->SetLineWidth(2);
    the_graph->SetMarkerColor(mycolour);
    the_graph->SetFillColor(mycolour);
    the_graph->Draw("C3");
    (theleg->AddEntry(the_graph,l_it->first,"L"))->SetLineWidth(8);
    ++i;
  }
  
  theleg->SetTextFont(42);
  theleg->Draw();
  
  std::cout << "Calling Draw()" << std::endl;
  return retval;
}

bool make_xsec_plot::AddObject(TString key, TString filename, bool relunc, bool ispb)
{
  Xsec l_obj;
  if (!l_obj.ReadFile(filename, relunc, ispb)) return false;
  m_map.emplace(key,l_obj);
  return true;
}

